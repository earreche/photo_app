Rails.application.routes.draw do
  devise_for :users, :controllers => { :registrations => 'registrations' }
  root 'welcome#index'
  get 'welcome/send_mail'
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
