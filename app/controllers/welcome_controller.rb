class WelcomeController < ApplicationController
  skip_before_action :authenticate_user!, only: [:index,:send_mail]

  def index
  end

  def send_mail
    UserMailer.new_order_email.deliver_now
    flash[:success] = "Parece que anduvo"
    render :index
  end

end
