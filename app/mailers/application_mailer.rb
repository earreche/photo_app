class ApplicationMailer < ActionMailer::Base
  default from: 'earreche@come.com.uy'
  layout 'mailer'
end
