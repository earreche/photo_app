class UserMailer < ApplicationMailer

  def new_order_email
    mail(to: "elmagoefrain@gmail.com", subject: "You got a new order!")
  end
end
